import 'package:devconsultation/daily_journal/domain/model/daily_journal.dart';
import 'package:devconsultation/daily_journal/domain/model/user.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test('User model fromJson should run with empty json', () async {
    User.fromJson(const {});
  });
  test('DailyJournal model fromJson should run with empty json', () async {
    DailyJournal.fromJson(const {});
  });
}
