import 'package:devconsultation/common/helper/extension/enum.dart';
import 'package:flutter_test/flutter_test.dart';

enum _TestingEnum {
  testing,
  testingData,
  testDataValue,
}

void main() {
  group('ValueExtension', () {
    test(
      'should get value as name converted to underscore case',
      () async {
        expect(_TestingEnum.testing.value, 'TESTING');
        expect(_TestingEnum.testingData.value, 'TESTING_DATA');
        expect(_TestingEnum.testDataValue.value, 'TEST_DATA_VALUE');
      },
    );
  });

  group('EnumByValue', () {
    test(
      'should get enum item from underscore case value',
      () async {
        expect(_TestingEnum.values.byValue('TESTING'), _TestingEnum.testing);
        expect(
          _TestingEnum.values.byValue('TESTING_DATA'),
          _TestingEnum.testingData,
        );
        expect(
          _TestingEnum.values.byValue('TEST_DATA_VALUE'),
          _TestingEnum.testDataValue,
        );
      },
    );
  });
}
