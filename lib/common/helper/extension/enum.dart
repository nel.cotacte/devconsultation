extension ValueExtension on Enum {
  String get value {
    return _toUnderscoreCase(name.split('.').last);
  }
}

extension EnumByValue<T extends Enum> on Iterable<T> {
  T? byValue(String valueToFind) {
    for (var enumItem in this) {
      if (enumItem.value == valueToFind) return enumItem;
    }

    return null;
  }
}

String _toUnderscoreCase(String? str) {
  if (str == null) return '';

  final RegExp exp = RegExp(r'(?<=[a-z])[A-Z]');

  return str
      .replaceAllMapped(exp, (Match m) => ('_${m.group(0) ?? ''}'))
      .toUpperCase();
}
