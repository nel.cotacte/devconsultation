import 'package:devconsultation/daily_journal/domain/model/user.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

@immutable
class DailyJournal extends Equatable {
  final String journalId;
  final User? user;
  final String report;

  const DailyJournal({
    required this.journalId,
    required this.user,
    required this.report,
  });

  DailyJournal copyWith({String? journalId, User? user, String? report}) {
    return DailyJournal(
      journalId: journalId ?? this.journalId,
      user: user ?? this.user,
      report: report ?? this.report,
    );
  }

  factory DailyJournal.fromJson(Map<String, dynamic> json) {
    return DailyJournal(
      journalId: json['journalId'] as String? ?? '',
      user: json['user'] == null ? null : User.fromJson(json['user']),
      //user: User.fromJson(json['user']),
      report: json['report'] as String? ?? '',
    );
  }

  @override
  List<Object?> get props {
    return [
      journalId,
      user,
      report,
    ];
  }
}
